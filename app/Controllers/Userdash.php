<?php

namespace App\Controllers;


class Userdash extends BaseController
{

    public function index()
    {
        $data = [
            'title' => 'Dashboard user'
        ];
        return view('user/dashboard', $data);
    }

    public function home()
    {
        $data = [
            'title' => 'Home user'
        ];

        return view('user/home', $data);
    }
}
