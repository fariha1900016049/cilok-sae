<?php

namespace App\Controllers;


class Admin extends BaseController
{

    public function index()
    {
        $data = [
            'title' => 'Dashboard admin'
        ];
        return view('admin/dashboard', $data);
    }

    public function home()
    {
        $data = [
            'title' => 'Home admin'
        ];

        return view('admin/home', $data);
    }
}
