<?php

namespace App\Controllers;

use App\Models\PesananModels;

class Pesanan extends BaseController
{
    // membuat variabel pesanan Model
    protected $pesananModel;
    public function __construct()
    {
        // memanggil produk models
        $this->pesananModel = new PesananModels();
    }

    public function index()
    {
        $data = [
            'title' => 'Data Pesanan',
            'pesanan' => $this->pesananModel->getAllPesanan(),
            'validation' => \Config\Services::validation()
        ];
        return view('admin/data-pesanan', $data);
    }

    public function daftar_pesanan()
    {
        $data = [
            'title' => 'Daftar Pesanan',
            'pesanan' => $this->pesananModel->getAllPesanan(),
        ];
        return view('user/daftar-pesanan', $data);
    }

    public function simpan()
    {
        // validasi input
        if (!$this->validate([
            'id' => [
                'rules' => 'required|is_unique[pesanan.id]',
                'errors' => [
                    'is_unique' => 'id sudah digunakan',
                    'required' => 'id pesanan harus diisi'
                ]
            ],
            'nama' => [
                'rules' => 'required|string',
                'errors' => [
                    'string' => 'field nama harus berisi huruf',
                    'required' => 'nama pesanan harus diisi'
                ]
            ],
            'harga' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'harga pesanan harus diisi'
                ]
            ],
            'foto' => [
                'rules' => 'uploaded[foto]|max_size[foto,1024]|is_image[foto]|mime_in[foto,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'uploaded' => 'foto pesanan harus diupload',
                    'max_size' => 'ukuran foto terlalu besar',
                    'is_image' => 'file harus berupa image',
                    'mime_in' => 'file harus berupa jpg, jpeg, png'
                ]
            ],
            'stok_barang' => [
                'rules' => 'required|integer',
                'errors' => [
                    'required' => 'field stok barang harus diisi',
                    'integer' => 'field stok barang harus berisi angka'
                ]
            ]
        ])) {
            // menampilkan pesan ketika error pada field
            session()->setFlashdata('error', $this->validator->listErrors());

            return redirect()->to('admin/data-pesanan')->withInput();
        }

        // mengambil data foto
        $fotoPesanan = $this->request->getFile('foto');

        // menentukan nama secara acak
        $namaPesanan = $fotoPesanan->getRandomName();

        // memindahkan image ke folder img
        $fotoPesanan->move('img', $namaPesanan);

        $data = [
            'id' => $this->request->getVar('id'),
            'nama' => $this->request->getVar('nama'),
            'harga' => $this->request->getVar('harga'),
            'foto' => $namaPesanan,
            'stok_barang' => $this->request->getVar('stok_barang'),
        ];

        $this->pesananModel->addPesanan($data);

        // menampilkan pesan ketika data berhasil ditambahkan
        session()->setFlashdata('berhasil', 'Data pesanan berhasil ditambahkan');

        return redirect()->to('admin/data-pesanan');
    }

    public function ubah()
    {
        if (!$this->validate([
            // validasi input
            'nama' => [
                'rules' => 'required|string',
                'errors' => [
                    'string' => 'field nama harus berisi huruf',
                    'required' => 'nama pesanan harus diisi'
                ]
            ],
            'harga' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'harga pesanan harus diisi'
                ]
            ],
            'foto' => [
                'rules' => 'max_size[foto,1024]|is_image[foto]|mime_in[foto,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'max_size' => 'ukuran foto terlalu besar',
                    'is_image' => 'file harus berupa image',
                    'mime_in' => 'file harus berupa jpg, jpeg, png'
                ]
            ],
            'stok_barang' => [
                'rules' => 'required|integer',
                'errors' => [
                    'required' => 'field stok barang harus diisi',
                    'integer' => 'field stok barang harus berisi angka'
                ]
            ]
        ])) {
            // menampilkan data ketika terjadi kesalahan
            session()->setFlashdata('error', $this->validator->listErrors());

            return redirect()->to('admin/data-pesanan')->withInput();
        }

        // mengamil data pada foto
        $fotoPesanan = $this->request->getFile('foto');

        // menamai gambar secara acak
        $namaPesanan = $fotoPesanan->getRandomName();

        // memindahkan gambar ke folder img
        $fotoPesanan->move('img', $namaPesanan);

        $id = $this->request->getVar('id');

        $data = [
            'nama' => $this->request->getVar('nama'),
            'harga' => $this->request->getVar('harga'),
            'foto' => $namaPesanan,
            'stok_barang' => $this->request->getVar('stok_barang')
        ];

        // method untuk update data
        $this->pesananModel->updatePesanan($id, $data);

        // menampilkan pesan ketika data berhasil di ubah
        session()->setFlashdata('ubah', 'Data pesanan berhasil diubah');

        return redirect()->to('admin/data-pesanan');
    }

    public function hapus()
    {
        // mengambil data id
        $data = [
            'id' => $this->request->getVar('id')
        ];

        // method untuk menghapus data
        $this->pesananModel->deletePesanan($data);

        // menampilkan pesan ketika data berhasil dihapus
        session()->setFlashdata('hapus', 'Pesanan berhasil dihapus');

        return redirect()->to('admin/data-pesanan');
    }
}
