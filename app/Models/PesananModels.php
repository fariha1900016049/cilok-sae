<?php

namespace App\Models;

use CodeIgniter\Model;

class PesananModels extends Model
{
    protected $table = 'pesanan';
    protected $allowedFields = ['id', 'nama', 'harga', 'foto', 'status', 'stok_barang'];
    protected $useTimestamps = true;
    protected $primaryKey = 'id';

    public function getAllPesanan()
    {
        return $this->findAll();
    }

    public function addPesanan($data)
    {
        return $this->db->table('pesanan')->insert($data);
    }

    public function updatePesanan($data, $id)
    {
        return $this->update($data, $id);
    }

    public function deletePesanan($data)
    {
        return $this->delete($data);
    }
}
