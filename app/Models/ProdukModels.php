<?php

namespace App\Models;

use CodeIgniter\Model;

class ProdukModels extends Model
{
    protected $table = 'produk';
    protected $allowedFields = ['id', 'nama', 'harga', 'foto', 'status', 'stok_produk'];
    protected $useTimestamps = true;
    protected $primaryKey = 'id';

    public function getAllProduk()
    {
        return $this->findAll();
    }

    public function addProduk($data)
    {
        return $this->db->table('produk')->insert($data);
    }

    public function updateProduk($data, $id)
    {
        return $this->update($data, $id);
    }

    public function deleteProduk($data)
    {
        return $this->delete($data);
    }
}
