<ul class="navbar-nav bg-gradient-danger sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('user/daftar-produk'); ?>">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-"></i>
        </div>
        <div class="sidebar-brand-text mx-3">CilokSae</div>
    </a>

    <!-- Divider Admin -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('admin/dashboard'); ?>">
        <a class="nav-link" href="<?= base_url('user/dashboard'); ?>">
            <i class="fas fa-fw fa-desktop"></i>
            <span>Dashboard</span></a
    </li>


    <!-- Divider User -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Pilihan
    </div>

    <?php if (in_groups('user')) : ?>
        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('user/daftar-produk'); ?>">
                <i class="fas fa-list-ul"></i>
                <span>Daftar Produk</span></a>
        </li>

         <!-- Nav Item - Dashboard -->
         <li class="nav-item">
            <a class="nav-link" href="<?= base_url('user/daftar-pesanan'); ?>">
                <i class="fas fa-list-ul"></i>
                <span>Daftar Pesanan</span></a>
        </li>
    <?php endif; ?>

    <?php if (in_groups('admin')) : ?>
        <!-- Nav Item - Tables -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('admin/data-produk'); ?>">
                <i class="fas fa-fw fa-table"></i>
                <span>Data Produk</span></a>
        </li>


            <!-- Nav Item - Tables -->
            <li class="nav-item">
            <a class="nav-link" href="<?= base_url('admin/data-pesanan'); ?>">
                <i class="fas fa-fw fa-users"></i>
                <span>Data Pesanan</span></a>
        </li>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('admin/data-transaksi'); ?>">
                <i class="fas fa-fw fa-users"></i>
                <span>Data Pembayaran</span></a>
        </li>
    <?php endif; ?>

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>