<?= $this->extend('templates/index'); ?>

<?= $this->section('page-content'); ?>

<div class="container-fluid">

    <div class="d-flex">
        <?php foreach ($pesanan as $prd) : ?>
            <div class="card m-3" style="width: 18rem;">
                <img class="card-img-top" src="/img/<?= $prd['foto']; ?>" alt="<?= $prd['nama']; ?>">
                <div class="card-body">
                    <h5 class="card-title"><b><?= $prd['nama']; ?></b></h5>
                    <p class="card-text">IDR <?= $prd['harga']; ?></p>
                    <a href="<?= base_url('user/daftar-produk'); ?>" class="btn btn-danger">Pesan Lagi</a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

</div>

<?= $this->endsection(); ?>