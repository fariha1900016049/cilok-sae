<?php $this->extend('templates/index'); ?>

<?php $this->section('page-content'); ?>


<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Pesanan</h6>
        </div>
        <div class="card-body">
            <button type="button" class="btn btn-success mb-3 mr-3" data-bs-toggle="modal" data-bs-target="#modalTambah">
                Tambah Data
            </button>
            <?php if (session()->getFlashdata('berhasil')) : ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->getFlashdata('berhasil'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <?php if (session()->getFlashdata('ubah')) : ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->getFlashdata('ubah'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <?php if (session()->getFlashdata('hapus')) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= session()->getFlashdata('hapus'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <?php if (session()->getFlashdata('error')) : ?>
                <div class="alert alert-warning pb-0" role="alert">
                    <?= session()->getFlashdata('error'); ?>
                </div>
            <?php endif; ?>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Harga Produk</th>
                            <th>Stok Produk</th>
                            <th>Foto</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <?php $i = 1 ?>
                    <?php foreach ($pesanan as $pesanan) : ?>
                        <tbody>
                            <tr>
                                <td><?= $i++; ?></td>
                                <td><?= $pesanan['id']; ?></td>
                                <td><?= $pesanan['nama']; ?></td>
                                <td><?= $pesanan['harga']; ?></td>
                                <td><?= $pesanan['stok_produk']; ?></td>
                                <td>
                                    <img style="width: 120px; height: 120px; object-fit: cover; border-radius: 100%;" src="/img/<?= $pesanan['foto']; ?>" alt="<?= $pesanan['nama']; ?>">
                                </td>
                                <td>
                                    <button type="button" id="btn-edit-pesanan" class="btn btn-warning mx-1" data-toggle="modal" data-target="#modalEdit" data-id="<?= $pesanan['id']; ?>" data-nama="<?= $pesanan['nama']; ?>" data-stok_barang="<?= $pesanan['stok_barang']; ?>" data-harga="<?= $pesanan['harga']; ?>">
                                        <i class="far fa-edit"></i></button>
                                    <button type="button" id="btn-hapus-pesanan" class="btn btn-danger mx-1" data-toggle="modal" data-target="#modalHapus" data-id="<?= $pesanan['id']; ?>">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- Modal tambah data produk-->
<div class="modal fade" id="modalTambah" tabindex="-1" aria-labelledby="modalTambah" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content p-2">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Produk</h5>
            </div>
            <div class="modal-body">
                <form action="/Pesanan/simpan" method="post" enctype="multipart/form-data" class="row g-3">
                    <?= csrf_field(); ?>
                    <div>
                        <label for="inputtext4" class="form-label">Kode Produk</label>
                        <input type="text" class="form-control" id="id" name="id">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Foto</label>
                        <input type="file" class="form-control" id="foto" name="foto">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Nama Produk</label>
                        <input type="text" class="form-control" id="nama" name="nama">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Stok Produk</label>
                        <input type="text" class="form-control" id="stok_barang" name="stok_barang">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Harga Produk</label>
                        <input type="text" class="form-control" id="harga" name="harga">
                    </div>

                    <div class="col-12 pt-2 form-group gap-3 d-flex justify-content-end">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit data produk-->
<div class="modal fade" id="modalEdit">
    <div class="modal-dialog">
        <div class="modal-content p-2">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data Produk</h5>
            </div>
            <div class="modal-body">
                <form action="/Pesanan/ubah" method="post" enctype="multipart/form-data" class="row g-3">
                    <?= csrf_field(); ?>
                    <div>
                        <input type="hidden" class="form-control" id="id" name="id">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Foto</label>
                        <input type="file" class="form-control" id="foto" name="foto">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Stok Barang</label>
                        <input type="text" class="form-control" id="stok_barang" name="stok_barang">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Harga Produk</label>
                        <input type="text" class="form-control" id="harga" name="harga">
                    </div>

                    <div class="col-12 pt-2 form-group gap-3 d-flex justify-content-end">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Ubah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus data produk-->
<div class="modal fade" id="modalHapus">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/Pesanan/hapus/<?= $pesanan['id']; ?>" method="post">
                <div class="modal-body">
                    Apakah anda yakin menghapus data ini?
                    <input type="hidden" name="id" id="id">
                </div>
                <div class="col-12 pt-2 form-group gap-3 d-flex justify-content-end">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->endSection(); ?>